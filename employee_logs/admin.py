from django.contrib import admin

from employee_logs.models import Task, Entry


class TaskAdmin(admin.ModelAdmin):
    list_display = ('text', 'date', 'owner')


class EntryAdmin(admin.ModelAdmin):
    list_display = ('task', 'text', 'date')


admin.site.register(Task, TaskAdmin)
admin.site.register(Entry, EntryAdmin)

