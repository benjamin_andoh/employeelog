from django.urls import path

from . import views

app_name = 'employee_logs'
urlpatterns = [
    path(r'', views.index, name='index'),

    path(r'main_tasks', views.main_tasks, name='main_tasks'),

    # path(r'end_date/(?P<task_id>\d+)/$', views.end_date, name="end_date"),

    path(r'sub_task/(?P<task_id>\d+)/$', views.sub_task, name='sub_task'),

    path(r'new_main_task', views.new_main_task, name='new_main_task'),

    # path(r'new_sub_task/(?P<task_id>\d+)\$', views.new_sub_task, name='new_sub_task'),

    path(r'edit_sub_task/(?P<entry_id>\d+)/$', views.edit_sub_task, name='edit_sub_task'),
]
