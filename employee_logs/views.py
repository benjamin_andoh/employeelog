from django.shortcuts import render
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from .models import Task, Entry
from .ModelsForm import TaskForm, EntryForm


def index(request):
    """the homepage"""
    return render(request, 'employee_logs/index.html')


@login_required
def main_tasks(request):
    main_tasks = Task.objects.filter(owner=request.user).order_by('date')
    context = {'main_tasks': main_tasks}
    return render(request, 'employee_logs/main_tasks.html', context)


@login_required
def sub_task(request, task_id):
    sub_task = Task.objects.get(id=task_id)
    # Make sure the task belong to the current user
    if sub_task.owner != request.user:
        print(" sorry this is not what a wrong page")

    entries = sub_task.entry_set.order_by('-date')
    context = {'sub_task': sub_task, 'entries': entries}
    return render(request, 'employee_logs/sub_task.html', context)


@login_required
def new_main_task(request):
    """this function add a new task"""
    if request.method == "POST":
        # POST data submitted; process data
        form = TaskForm(request.POST)
        if form.is_valid():
            new_topic = form.save(commit=False)
            new_topic.owner = request.user
            new_topic.save()
            return HttpResponseRedirect(reverse('employee_logs:main_tasks'))
    else:
        # No data submitted; create a blank form.
        form = TaskForm()

    context = {'form': form}
    return render(request, 'employee_logs/new_main_task.html', context)


@login_required
def edit_sub_task(request, entry_id):
    """Editing a sub ta sk"""
    entry = Entry.objects.get(id=entry_id)
    task = entry.task
    if task.owner != request.user:
        raise Http404

    if request.method == 'POST':
        # POST data submitted; process data.
        form = EntryForm(instance=entry, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('employee_logs:sub_task', args=[task.id]))
    else:
        form = EntryForm(instance=entry)

    context = {'entry': entry, 'task': task, 'form': form}
    return render(request, 'employee_logs/edit_sub_task.html', context)




